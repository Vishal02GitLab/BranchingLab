package com.nTier.events.rentals;
import java.io.Serializable;


public class Guest implements Serializable, Comparable<Guest> {
	private String firstName;
	private String lastName;
	private String hotelClubNumber;
	private String rentalCarClubNumber;
	private String driversLicenseNumber;
	private String frequentFlierNumber;
	
	private static final long serialVersionUID = 1L;
	
	public Guest() {
		
	}
	
	public Guest(String firstName, String lastName, String hotelClubNumber,
			String rentalCarClubNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.hotelClubNumber = hotelClubNumber;
		this.rentalCarClubNumber = rentalCarClubNumber;
	}

	public Guest(String firstName, String lastName, String hotelClubNumber,
			String rentalCarClubNumber, String driversLicensNum) {
		this(firstName, lastName, hotelClubNumber, rentalCarClubNumber);
		this.setDriversLicenseNumber(driversLicensNum);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("First Name=");
		builder.append(firstName);
		builder.append("\nLast Name=");
		builder.append(lastName);
		builder.append("\nHotel Club Number=");
		builder.append(hotelClubNumber);
		builder.append("\nRental Car Club Number=");
		builder.append(rentalCarClubNumber);
		builder.append("\nLicenseNumber=");
		builder.append(driversLicenseNumber);
		builder.append("\nFrequent Flyer Number=");
		builder.append(frequentFlierNumber);
		return builder.toString();
	}
	
	
	public String getFrequentFlierNumber() {
		return frequentFlierNumber;
	}

	public void setFrequentFlierNumber(String frequentFlierNumber) {
		this.frequentFlierNumber = frequentFlierNumber;
	}

	public String getDriversLicenseNumber() {
		return driversLicenseNumber;
	}

	public void setDriversLicenseNumber(String driversLicenseNumber) {
		this.driversLicenseNumber = driversLicenseNumber;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getHotelClubNumber() {
		return hotelClubNumber;
	}
	public void setHotelClubNumber(String hotelClubNumber) {
		this.hotelClubNumber = hotelClubNumber;
	}
	public String getRentalCarClubNumber() {
		return rentalCarClubNumber;
	}
	public void setRentalCarClubNumber(String rentalCarClubNumber) {
		this.rentalCarClubNumber = rentalCarClubNumber;
	}

	@Override
	public int compareTo(Guest o) {
		return this.getLastName().compareTo(o.getLastName());
		
	}
	
	
}
