package com.nTier.events.rentals;

public class HotelRoom implements Rentable {
	private int roomNumber;
	private boolean smoking;
	private int numberOfBeds = 1; // set to default value
	
	
	 public HotelRoom(int roomNumber) {
		this.setRoomNumber(roomNumber);
	}
	
	 public int getNumberOfBeds() {
		return numberOfBeds;
	}

	 void setNumberOfBeds(int numberOfBeds) {
		if (numberOfBeds > 0 && numberOfBeds <3){
			this.numberOfBeds = numberOfBeds;
		}
	}

	 public String toString(){
		return "room: " + "roomnumber=" + getRoomNumber()
				+ " smoking preference=" + isSmoking() + " numberOfBeds=" + 
				getNumberOfBeds();
	}
	 
	 @Override
		public boolean equals(Object obj) {
			boolean theyRequal = false;
			if (this == obj) { //if they point to same place on heap
				theyRequal = true;
			} else if (obj != null) {
				HotelRoom other = (HotelRoom) obj;
				if (this.getRoomNumber() == other.getRoomNumber()) {  //if roomNumbers are the same
					theyRequal = true;
				}
			}	
			return theyRequal;
		}
	
	 public int getRoomNumber() {
		return roomNumber;
	}
	 public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	 public boolean isSmoking() {
		return smoking;
	}
	 public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	
	
}
