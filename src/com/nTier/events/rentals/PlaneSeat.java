package com.nTier.events.rentals;

public class PlaneSeat implements Rentable {

	// data members
	private String seatNumber;
	private String departureCode;
	private String arrivalCode;

	// constructors
	public PlaneSeat() {
	}

	public PlaneSeat(String seatNumber) {
		this.setSeatNumber(seatNumber);
	}

	public PlaneSeat(String seatNumber, String departureCode, String arrivalCode) {
		this(seatNumber);
		this.setDepartureCode(departureCode);
		this.setArrivalCode(arrivalCode);
	}

	// accessor methods
	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public String getDepartureCode() {
		return departureCode;
	}

	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}

	public String getArrivalCode() {
		return arrivalCode;
	}

	public void setArrivalCode(String arrivalCode) {
		this.arrivalCode = arrivalCode;
	}

	// returns a string representation of the data
	public String toString() {
		return "PlaneSeat:  seatNumber=" + this.getSeatNumber()
				+ "\tdepartureCode=" + this.getDepartureCode() + "\tarrivalCode="
				+ this.getArrivalCode();
	}

	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof PlaneSeat) {
			PlaneSeat other = (PlaneSeat) obj;
			result = this.getSeatNumber().equals(other.getSeatNumber())
					&& this.getDepartureCode().equals(other.getDepartureCode())
					&& this.getArrivalCode().equals(other.getArrivalCode());
		}
		return result;
	}
}
