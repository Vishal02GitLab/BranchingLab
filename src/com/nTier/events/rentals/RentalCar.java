package com.nTier.events.rentals;

public class RentalCar implements Rentable {
	private long id;
	private String make;
	private String model;
	private int year = 2009;
	
	//added for 6.2 lab
	 public RentalCar(long id){
		this.setId(id);
	}
	
	 int getYear() {
		return year;
	}

	 void setYear(int year) {
		this.year = year;
	}

	 public String toString(){
		return "car id: " + getId() + " - Make:" + getMake()
				+ " - Model:" + getModel() + " Year: " + getYear();
	}
	 
	 @Override
		public boolean equals(Object obj) {
			boolean theyRequal = false;
			if (this == obj) { //if they point to same place on heap
				theyRequal = true;
			} else if (obj != null) {
				RentalCar other = (RentalCar) obj;
				if (this.getId() == other.getId()) {  //if ID's are the same
					theyRequal = true;
				}
			}	
			return theyRequal;
		}
	
	 public long getId() {
		return id;
	}
	 public void setId(long id) {
		this.id = id;
	}
	 public String getMake() {
		return make;
	}
	 public void setMake(String make) {
		this.make = make;
	}
	 public String getModel() {
		return model;
	}
	 public void setModel(String model) {
		this.model = model;
	}
	
	
}
