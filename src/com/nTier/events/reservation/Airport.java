package com.nTier.events.reservation;

public class Airport {
	private String code;
	private String name;
	private String city;
	private String state;

	public Airport() {
	}

	public Airport(String code, String name, String city, String state) {
		this.setCode(code);
		this.setName(name);
		this.setCity(city);
		this.setState(state);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}