package com.nTier.events.reservation;

import com.nTier.events.rentals.Guest;
import com.nTier.events.rentals.PlaneSeat;
import com.wrongsisters.AirRides;
import com.wrongsisters.AirportLookup;

class PlaneReservation extends Reservation {
	private PlaneSeat seat;

	PlaneReservation() {
	}

	PlaneReservation(Guest guest) {
		super(guest);
	}

	PlaneReservation(Guest guest, PlaneSeat seat) {
		this(guest);
		this.setSeat(seat);
	}

	/*
	 * Makes plane reservation. Uses 3rd party class to verify airport codes. Guest's frequent flier number must also be valid.
	 */
	public boolean reserve() {
		boolean result = false;

		// check airport codes via AirRides
		int reservationCode = AirRides.reserve(this.getSeat().getDepartureCode(), this.getSeat().getArrivalCode());

		// if AirRides indicates success, check guest's frequent flier number
		if (reservationCode == 1) {
			result = this.validateClubNumber();
		}
		return result;
	}

	/*
	 * Validates guest's frequent flier club number, using AirRides class
	 */
	boolean validateClubNumber() {
		return AirRides.validateFrequentFlierNumber(this.getGuest().getFrequentFlierNumber());
	}

	/*
	 * Returns departure Airport. NOTE: this is derived data, do not store an Airport object here.
	 */
	public Airport getDepartureAirport() {
		return this.getAirport(this.getSeat().getDepartureCode());
	}

	/*
	 * Returns arrival Airport. NOTE: this is derived data, do not store an Airport object here. Calling Client displays as they
	 * want to.
	 */
	public Airport getArrivalAirport() {
		return this.getAirport(this.getSeat().getArrivalCode());
	}

	/*
	 * Returns Airport for given airport code (null if code invalid/not found).
	 */
	private Airport getAirport(String airportCode) {
		Airport result = null;
		String airportData = AirportLookup.lookup(airportCode);

		// if airportCode found, create and return Airport from the data
		if (!AirportLookup.UNKNOWN_AIRPORT.equals(airportData)) {
			String[] fields = airportData.split("\\|"); // regex
			result = new Airport(fields[0], fields[1], fields[2], fields[3]);
		}
		return result;
	}

	public PlaneSeat getSeat() {
		return seat;
	}

	public void setSeat(PlaneSeat seat) {
		this.seat = seat;
	}
}
