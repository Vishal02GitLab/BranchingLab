package com.nTier.events.reservation;

public class ReservationException extends Exception {

	private static final long serialVersionUID = 1L;

	public ReservationException() {
		// TODO Auto-generated constructor stub
	}

	public ReservationException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ReservationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ReservationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
